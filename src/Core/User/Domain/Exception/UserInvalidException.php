<?php

namespace App\Core\User\Domain\Exception;

class UserInvalidException extends UserException
{
}
