<?php

namespace App\Core\User\Application\Validation;

use App\Core\User\Domain\Exception\UserAlreadyExistsException;
use App\Core\User\Domain\Exception\UserInactiveException;
use App\Core\User\Domain\Exception\UserInvalidException;
use App\Core\User\Domain\Repository\UserRepositoryInterface;
use App\Core\User\Domain\User;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserValidator
{
    public function __construct(
        private readonly UserRepositoryInterface $userRepository,
        private readonly ValidatorInterface $validator
    ){
    }

    public function validateActivity(User $user): void
    {
        if (!$user->isActive()) {
            throw new UserInactiveException(sprintf('Użytkownik %s jest niekatywny.', $user->getEmail()));
        }
    }

    public function validateEmailExists(string $email): void
    {
        if (!$this->userRepository->checkEmailAvailability($email)) {
            throw new UserAlreadyExistsException('Istnieje już użytkownik z podanym adresem email');
        }
    }

    public function validateEntity(User $user): void
    {
        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            $errorsString = (string) $errors;

            throw new UserInvalidException($errorsString);
        }
    }
}
