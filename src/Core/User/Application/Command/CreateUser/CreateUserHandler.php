<?php

namespace App\Core\User\Application\Command\CreateUser;

use App\Core\User\Application\Validation\UserValidator;
use App\Core\User\Domain\Repository\UserRepositoryInterface;
use App\Core\User\Domain\User;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class CreateUserHandler
{
    public function __construct(
        private readonly UserRepositoryInterface $userRepository,
        private readonly UserValidator $userValidator,
    ) {}

    public function __invoke(CreateUserCommand $command): void
    {
        $this->userValidator->validateEmailExists($command->email);
        $user = new User($command->email);
        $this->userValidator->validateEntity($user);

        $this->userRepository->save($user);
        $this->userRepository->flush();
    }
}
